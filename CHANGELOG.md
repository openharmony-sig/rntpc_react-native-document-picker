# Changelog

## v9.2.1-rc.1
- react-native-document-picker 库从github到gitee迁移 ([b1f5a001](https://gitee.com/openharmony-sig/rntpc_react-native-document-picker/commit/b1f5a001e352bde86ea0661183c1a15db3f43c90))
- feat：Modify the codegen-lib script and regenerate codegen files ([1e5888c4](https://gitee.com/openharmony-sig/rntpc_react-native-document-picker/commit/1e5888c4c52995143baf57dfb3aa8d93a3d848f2))
- pre-release: @react-native-ohos/react-native-document-picker@9.2.1-rc.1 ([e500fd52](https://gitee.com/openharmony-sig/rntpc_react-native-document-picker/commit/e500fd5263773fe0cbb3fa196ea22f2ef24c497b))
