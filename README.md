# @react-native-ohos/react-native-document-picker
This project is based on  [react-native-document-picker](https://github.com/react-native-documents/document-picker)
## Documentation
- [中文](https://gitee.com/react-native-oh-library/usage-docs/blob/master/zh-cn/react-native-document-picker.md)

- [English](https://gitee.com/react-native-oh-library/usage-docs/blob/master/en/react-native-document-picker.md)

## License
This library is licensed under [The MIT License (MIT)](https://gitee.com/openharmony-sig/rntpc_react-native-document-picker/blob/master/LICENSE.md).
